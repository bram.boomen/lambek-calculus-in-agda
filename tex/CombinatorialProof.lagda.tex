% Created 2019-05-06 ma 14:21
% Intended LaTeX compiler: xelatex
\documentclass[a4paper, 11pt]{article}
    \input{/home/bram/.dotfiles/latex/agda.tex}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usetag{tutorial,comment,note}
\author{Bram van den Boomen}
\date{\today}
\title{CombinatorialProof Module}
\hypersetup{
 pdfauthor={Bram van den Boomen},
 pdftitle={CombinatorialProof Module},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.2 (Org mode 9.1.9)}, 
 pdflang={English}}
\usepackage[conor]{agda}

\begin{document}

\maketitle
\emph{version: Main \tagged{tutorial}{+ Tutorial} \tagged{note}{+ Notes}}

\section{Preface}
\label{sec:org5c171ec}
\begin{code}
module CombinatorialProof where

open import Logic
\end{code}

\section{Combinatorial Proofs}
\label{sec:orgb06881c}

We start the module by defining what a combinatorial proof (CProof hereafter) is. We can define it as a datatype with a constructor that takes a \texttt{Type} as precedent and a \texttt{Type} as antecedent.

\begin{code}
infix 4 NLc_ Lc_ LPc_ SCs_
infix 5 _↣_
data Combinatorial : Set where
  _↣_ : Type → Type → Combinatorial
\end{code}

As our current definition allows áll combinations of \texttt{Type} to construct a proof, we can now introduce some constraints in the form of logical rules by defining a type indexed over CProofs.
The constructors of this type are very straightforward, they correspond 1-1 with the logical rules of the Combinatorial calculus:

\begin{tutorial}
For the axiom (\texttt{I}) constructor, we could leave the argument \texttt{A} implicit, however, it is nice to know which axioms are used in a particular proof. We could be even more explicit and have the arguments to the other constructors be explicit as well. This would not be very readable however and it would be more desirable to have a function that exports a proof to LaTeX instead.
\end{tutorial}

\subsection{NL (base logic)}
\label{sec:orgc17c8c8}
\newcommand{\arr}[2]{#1 \rightarrow #2}
\newcommand{\bs}{\backslash}
\newcommand{\fs}{/}
\[\addtolength{\inferLineSkip}{1pt}
\infer[I]{A \rightarrow A}{} \qquad \infer[\circ]{A \rightarrow C}{A \rightarrow B & B \rightarrow C}\]
%
\[\addtolength{\inferLineSkip}{1pt}
\infer[\triangleright]{A \rightarrow C \fs B}{A\otimes B \rightarrow C}\quad
\infer[\triangleright`]{A\otimes B \rightarrow C}{A \rightarrow C \fs B}\qquad
\infer[\triangleleft]{B \rightarrow A\bs C}{A\otimes B \rightarrow C}\quad
\infer[\triangleleft`]{A\otimes B \rightarrow C}{B \rightarrow A\bs C}
\]

\begin{code}
data NLc_ : Combinatorial → Set where
  I  : ∀ (A    ) → NLc A ↣ A
  ∘  : ∀ {A B C} → NLc A ↣ B → NLc B ↣ C → NLc A ↣ C
  ▹  : ∀ {A B C} → NLc A ⊗ B ↣ C → NLc A ↣ C ⟪ B
  ▹′ : ∀ {A B C} → NLc A ↣ C ⟪ B → NLc A ⊗ B ↣ C
  ◃  : ∀ {A B C} → NLc A ⊗ B ↣ C → NLc B ↣ A ⟫ C
  ◃‵ : ∀ {A B C} → NLc B ↣ A ⟫ C → NLc A ⊗ B ↣ C
  ⊗m  : ∀ {A B C D} → NLc A ↣ B → NLc C ↣ D → NLc A ⊗ C ↣ B ⊗ D
  ⟪m  : ∀ {A B C D} → NLc A ↣ B → NLc C ↣ D → NLc A ⟪ D ↣ B ⟪ C
  ⟫m  : ∀ {A B C D} → NLc A ↣ B → NLc C ↣ D → NLc D ⟫ A ↣ C ⟫ B
\end{code}

\subsection{L (associativity)}
\label{sec:orgfc83323}
\textbf{L}=\textbf{NL}+ass$^{l}$,ass$^{r}$

\[\addtolength{\inferLineSkip}{1pt}
\infer[\textsf{ass}^{l}]{(A\otimes B)\otimes C \rightarrow D}{A\otimes(B\otimes C) \rightarrow D}
\qquad
\infer[\textsf{ass}^{r}]{A\otimes(B\otimes C) \rightarrow D}{(A\otimes B)\otimes C \rightarrow D}
\]

\begin{code}
data Lc_ : Combinatorial → Set where
  I    : ∀ (A    ) → Lc A ↣ A
  ∘    : ∀ {A B C} → Lc A ↣ B → Lc B ↣ C → Lc A ↣ C
  ▹    : ∀ {A B C} → Lc A ⊗ B ↣ C → Lc A ↣ C ⟪ B
  ▹′   : ∀ {A B C} → Lc A ↣ C ⟪ B → Lc A ⊗ B ↣ C
  ◃    : ∀ {A B C} → Lc A ⊗ B ↣ C → Lc B ↣ A ⟫ C
  ◃‵   : ∀ {A B C} → Lc B ↣ A ⟫ C → Lc A ⊗ B ↣ C
  ⊗m   : ∀ {A B C D} → Lc A ↣ B → Lc C ↣ D → Lc A ⊗ C ↣ B ⊗ D
  ⟪m   : ∀ {A B C D} → Lc A ↣ B → Lc C ↣ D → Lc A ⟪ D ↣ B ⟪ C
  ⟫m   : ∀ {A B C D} → Lc A ↣ B → Lc C ↣ D → Lc D ⟫ A ↣ C ⟫ B
  assᵣ : ∀ {A B C D} → Lc A ⊗ (B ⊗ C) ↣ D → Lc (A ⊗ B) ⊗ C ↣ D
  assₗ : ∀ {A B C D} → Lc (A ⊗ B) ⊗ C ↣ D → Lc A ⊗ (B ⊗ C) ↣ D
\end{code}

\subsection{LP (commutativity)}
\label{sec:org6dec1af}
\textbf{LP}=\textbf{L}+comm

\paragraph*{Commutativity}
\[\addtolength{\inferLineSkip}{1pt}
\infer[\textsf{comm}]{A\otimes B \rightarrow C}{B\otimes A \rightarrow C}
\]

\begin{code}
data LPc_ : Combinatorial → Set where
  I    : ∀ (A    ) → LPc A ↣ A
  ∘    : ∀ {A B C} → LPc A ↣ B → LPc B ↣ C → LPc A ↣ C
  ▹    : ∀ {A B C} → LPc A ⊗ B ↣ C → LPc A ↣ C ⟪ B
  ▹′   : ∀ {A B C} → LPc A ↣ C ⟪ B → LPc A ⊗ B ↣ C
  ◃    : ∀ {A B C} → LPc A ⊗ B ↣ C → LPc B ↣ A ⟫ C
  ◃‵   : ∀ {A B C} → LPc B ↣ A ⟫ C → LPc A ⊗ B ↣ C
  ⊗m   : ∀ {A B C D} → LPc A ↣ B → LPc C ↣ D → LPc A ⊗ C ↣ B ⊗ D
  ⟪m   : ∀ {A B C D} → LPc A ↣ B → LPc C ↣ D → LPc A ⟪ D ↣ B ⟪ C
  ⟫m   : ∀ {A B C D} → LPc A ↣ B → LPc C ↣ D → LPc D ⟫ A ↣ C ⟫ B
  assᵣ : ∀ {A B C D} → LPc A ⊗ (B ⊗ C) ↣ D → LPc (A ⊗ B) ⊗ C ↣ D
  assₗ : ∀ {A B C D} → LPc (A ⊗ B) ⊗ C ↣ D → LPc A ⊗ (B ⊗ C) ↣ D
  comm : ∀ {A B C} → LPc B ⊗ A ↣ C → LPc A ⊗ B ↣ C
\end{code}

\subsection{SC (structural control)}
\label{sec:org89e72d0}

\textbf{SC}=\textbf{NL}+\(\triangledown\Diamond+\triangledown'\Diamond+\Diamond \textsf{ass}^{l}+\Diamond \textsf{ass}^{r}+\Diamond \textsf{comm}^{l}+\Diamond \textsf{comm}^{r}\)

\[\addtolength{\inferLineSkip}{1pt}
\infer[\triangledown]{A \rightarrow \Box B}{\Diamond A \rightarrow B} \qquad
\infer[\triangledown']{\Diamond A \rightarrow B}{A \rightarrow \Box B}
\]
%
\[\addtolength{\inferLineSkip}{1pt}
\infer[\Diamond\textsf{ass}^{l}]{(\Diamond A \otimes B) \otimes C \rightarrow D}{\Diamond A \otimes (B \otimes C) \rightarrow D} \quad
\infer[\Diamond\textsf{ass}^{r}]{A \otimes (B \otimes \Diamond C) \rightarrow D}{(A \otimes B) \otimes \Diamond C \rightarrow D}
\]
%
\[\addtolength{\inferLineSkip}{1pt}
\infer[\Diamond\textsf{comm}^{l}]{(A \otimes \Diamond C) \otimes B) \rightarrow D}{(A \otimes B) \otimes \Diamond C \rightarrow D} \quad
\infer[\Diamond\textsf{comm}^{r}]{B \otimes (\Diamond A \otimes B) \rightarrow D}{\Diamond A \otimes (B \otimes C) \rightarrow D}
\]

\begin{code}
data SCs_ : Combinatorial → Set where
  I     : ∀ (A      ) → SCs A ↣ A
  ∘     : ∀ {A B C  } → SCs A ↣ B → SCs B ↣ C → SCs A ↣ C
  ▹     : ∀ {A B C  } → SCs A ⊗ B ↣ C → SCs A ↣ C ⟪ B
  ▹`    : ∀ {A B C  } → SCs A ↣ C ⟪ B → SCs A ⊗ B ↣ C
  ◃     : ∀ {A B C  } → SCs A ⊗ B ↣ C → SCs B ↣ A ⟫ C
  ◃`    : ∀ {A B C  } → SCs B ↣ A ⟫ C → SCs A ⊗ B ↣ C
  ▿     : ∀ {A B    } → SCs ◇ A ↣ B → SCs A ↣ □ B
  ▿`    : ∀ {A B    } → SCs A ↣ □ B → SCs ◇ A ↣ B
  ◇assˡ : ∀ {A B C D} → SCs ◇ A ⊗ (B ⊗ C) ↣ D → SCs (◇ A ⊗ B) ⊗ C ↣ D
  ◇assʳ : ∀ {A B C D} → SCs (A ⊗ B) ⊗ ◇ C ↣ D → SCs A ⊗ (B ⊗ ◇ C) ↣ D
  ◇comˡ : ∀ {A B C D} → SCs (A ⊗ B) ⊗ ◇ C ↣ D → SCs (A ⊗ ◇ C) ⊗ B ↣ D
  ◇comʳ : ∀ {A B C D} → SCs ◇ A ⊗ (B ⊗ C) ↣ D → SCs B ⊗ (◇ A ⊗ C) ↣ D
\end{code}
\end{document}
