% Created 2019-05-06 ma 14:17
% Intended LaTeX compiler: xelatex
\documentclass[a4paper, 11pt]{article}
    \input{/home/bram/.dotfiles/latex/agda.tex}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usetag{tutorial,note}
\usepackage{newunicodechar}
\newunicodechar{⟨}{\ensuremath{\langle}}
\newunicodechar{⟩}{\ensuremath{\rangle}}
\author{Bram van den Boomen}
\date{\today}
\title{NDProof Module}
\hypersetup{
 pdfauthor={Bram van den Boomen},
 pdftitle={NDProof Module},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.2 (Org mode 9.1.9)}, 
 pdflang={English}}
\usepackage[conor]{agda}

\begin{document}

\maketitle

\section{Preface}
\label{sec:orgdac1423}
\emph{Version: Main \tagged{tutorial}{+ Tutorial} \tagged{note}{+ Notes}}

⟨ Z ⟩

\begin{code}
open import Agda.Builtin.Equality using (_≡_; refl)

module NDProof where
open import Logic
\end{code}


\begin{code}
infix 4 NLn_ Ln_ LPn_ SCn_
infix 5 _⊢_
data Natural : Set where
  _⊢_ : Struct → Type → Natural
\end{code}

\section{NL (base logic)}
\label{sec:orgce26d17}

\begin{tutorial}
A note on the \texttt{X[A]} notation: This notation means \texttt{A} is a substructure of \texttt{X}, which should be easy enough to formalize using the \texttt{∈s} function. However, when applied as inference rules, this notation leaves something implicit: When rewriting \texttt{X[A]} to \texttt{X}, we mean `\texttt{X} is the remainder of pruning structure \texttt{A} from \texttt{X[A]}' and when rewriting \texttt{X[A]} to \texttt{X[B]} we mean `\texttt{X[B]} is the structure \texttt{X} with \texttt{B} added, after pruning \texttt{A} from \texttt{X}. In Agda, we cannot leave this implicit, we need to provide a proof that \texttt{X[A]}, after pruning \texttt{A}, is equal to \texttt{X[B]} after pruning \texttt{B}. The \texttt{⊗E} constructor therefore needs 3 proofs:
\begin{itemize}
\item \texttt{\{s1 : A ∙ B ∈s X[A∙B]\}}\\
proof that \texttt{A ∙ B} is a substructure of \texttt{X}.
\item \texttt{\{s2 : Y ∈s X[Y]\}}\\
proof that \texttt{Y} is a substructure of \texttt{X}.
\item \texttt{(s3 : X[A∙B] ─ A ∙ B ≡ X[Y] ─ Y)}\\
proof that \texttt{X[A∙B]} after pruning \texttt{A ∙ B} is equal to \texttt{X[Y]} after pruning \texttt{Y}. To prune a substructure from a structure, we need a proof that that substructure is indeed a substructure of that structure, which we have implicitly in \texttt{s1} and \texttt{s2}.
\end{itemize}
\end{tutorial}
\begin{note}
Ideally, we would leave ther arguments \texttt{s1, s2 and s3} implicit, especially because the only valid proof for s3 is \texttt{refl}. However, because \texttt{s3} is not actually used in the explicit part of the proof, Agda is happy to skip it when type-checking. Note that \texttt{s1} and \texttt{s2} are used in an explicit part, so these arguments can be left implicit. We can do away with the \texttt{refl} overhead by defining a pattern:\\
\texttt{pattern ⊗E' p q = ⊗E refl p q}
\end{note}

\subsection{Logical Rules}
\label{sec:org97f367f}
\newcommand{\arr}[2]{#1 \rightarrow #2}
\newcommand{\nd}[2]{#1 \vdash #2}
\renewcommand{\to}{\vdash}
\newcommand{\bs}{\backslash}
\newcommand{\fs}{/}

\[\addtolength{\inferLineSkip}{1pt}
\infer[\bs E]{\nd{X\cdot Y}{B}}{\nd{X}{A} & \nd{Y}{A\bs B}}
\qquad
\infer[\slash E]{\nd{X\cdot Y}{B}}{\nd{X}{B \fs A} & \nd{Y}{A}}
\]
%
\[\addtolength{\inferLineSkip}{1pt}
\infer[\bs I]{\nd{X}{A\bs B}}{\nd{A\cdot X}{B}}
\qquad
\infer[\slash I]{\nd{X}{B \fs A}}{\nd{X\cdot A}{B}}
\]
%
\[\addtolength{\inferLineSkip}{1pt}
\infer[\otimes I]{\nd{X\cdot Y}{A\otimes B}}{\nd{X}{A} & \nd{Y}{B}}
\qquad
\infer[\otimes E]{\nd{X[Y]}{C}}{\nd{Y}{A\otimes B} & \nd{X[A\cdot B]}{C}}
\]

\subsection{Agda Rules}
\label{sec:org38681bf}
\begin{code}
data NLn_ : Natural → Set where
  ax : (A : Struct) → NLn A ⊢ [ A ]ᵀ
  ⟪E : ∀ {A B   X Y} → NLn X ⊢ B ⟪ A → NLn Y ⊢ A → NLn X ∙ Y ⊢ B
  ⟫E : ∀ {A B   X Y} → NLn X ⊢ A → NLn Y ⊢ A ⟫ B → NLn X ∙ Y ⊢ B
  ⊗E : ∀ {A B C   Y}{X[A∙B] X[Y]}{s1 : (A ∙ B) ∈s X[A∙B]}{s2 : Y ∈s X[Y]}
    → (s3 : ((X[A∙B] ─ (A ∙ B)) s1) ≡ ((X[Y] ─ Y) s2))
    → NLn Y ⊢ [ A ]ᵀ ⊗ [ B ]ᵀ → NLn X[A∙B] ⊢ C → NLn X[Y] ⊢ C
  ⟪I : ∀ {A B   X  } → NLn X ∙ A ⊢ B → NLn X ⊢ B ⟪ [ A ]ᵀ
  ⟫I : ∀ {A B   X  } → NLn A ∙ X ⊢ B → NLn X ⊢ [ A ]ᵀ ⟫ B
  ⊗I : ∀ {A B   X Y} → NLn X ⊢ A → NLn Y ⊢ B → NLn X ∙ Y ⊢ A ⊗ B
\end{code}

\section{L (associativity)}
\label{sec:orgcb79bdc}

\subsection{Logical Rules}
\label{sec:org3ec376a}
\textbf{L}=\textbf{NL}+ass$^{l}$,ass$^{r}$

\[\addtolength{\inferLineSkip}{1pt}
\infer[\texttt{ass}^l]{X[(Y\cdot Z)\cdot W] \to D}{X[Y \cdot (Z \cdot W)] \to D}
\qquad
\infer[\texttt{ass}^r]{X[Y\cdot (Z\cdot W)] \to D}{X[(Y \cdot Z) \cdot W] \to D}
\]

\subsection{Agda Rules}
\label{sec:org45b935f}
\begin{code}
data Ln_ : Natural → Set where
  ax : (A : Struct) → Ln A ⊢ [ A ]ᵀ
  ⟪E : ∀ {A B   X Y} → Ln X ⊢ B ⟪ A → Ln Y ⊢ A → Ln X ∙ Y ⊢ B
  ⟫E : ∀ {A B   X Y} → Ln X ⊢ A → Ln Y ⊢ A ⟫ B → Ln X ∙ Y ⊢ B
  ⊗E : ∀ {A B C   Y}{X[A∙B] X[Y]}{s1 : (A ∙ B) ∈s X[A∙B]}{s2 : Y ∈s X[Y]}
    → (s3 : ((X[A∙B] ─ (A ∙ B)) s1) ≡ ((X[Y] ─ Y) s2))
    → Ln Y ⊢ [ A ]ᵀ ⊗ [ B ]ᵀ → Ln X[A∙B] ⊢ C → Ln X[Y] ⊢ C
  ⟪I : ∀ {A B   X  } → Ln X ∙ A ⊢ B → Ln X ⊢ B ⟪ [ A ]ᵀ
  ⟫I : ∀ {A B   X  } → Ln A ∙ X ⊢ B → Ln X ⊢ [ A ]ᵀ ⟫ B
  ⊗I : ∀ {A B   X Y} → Ln X ⊢ A → Ln Y ⊢ B → Ln X ∙ Y ⊢ A ⊗ B

  assᵣ : ∀ {Y Z W D XR XL}
    {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
    {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
    (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
    → Ln XL ⊢ D → Ln XR ⊢ D
  assₗ : ∀ {Y Z W D XR XL}
    {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
    {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
    (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
    → Ln XR ⊢ D → Ln XL ⊢ D
\end{code}

\section{LP (commutativity)}
\label{sec:org0696898}

\subsection{Logical Rules}
\label{sec:orge7c3d26}
\textbf{LP}=\textbf{L}+comm

\[
\infer[\texttt{comm}]{X[Y\cdot Z] \to C}{X[Z\cdot Y] \to C}
\]

\subsection{Agda Rules}
\label{sec:org197ce08}
\begin{code}
data LPn_ : Natural → Set where
  nln  : ∀ {Π} → Ln Π → LPn Π
  ax : (A : Struct) → LPn A ⊢ [ A ]ᵀ
  ⟪E : ∀ {A B   X Y} → LPn X ⊢ B ⟪ A → LPn Y ⊢ A → LPn X ∙ Y ⊢ B
  ⟫E : ∀ {A B   X Y} → LPn X ⊢ A → LPn Y ⊢ A ⟫ B → LPn X ∙ Y ⊢ B
  ⊗E : ∀ {A B C   Y}{X[A∙B] X[Y]}{s1 : (A ∙ B) ∈s X[A∙B]}{s2 : Y ∈s X[Y]}
    → (s3 : ((X[A∙B] ─ (A ∙ B)) s1) ≡ ((X[Y] ─ Y) s2))
    → LPn Y ⊢ [ A ]ᵀ ⊗ [ B ]ᵀ → LPn X[A∙B] ⊢ C → LPn X[Y] ⊢ C
  ⟪I : ∀ {A B   X  } → LPn X ∙ A ⊢ B → LPn X ⊢ B ⟪ [ A ]ᵀ
  ⟫I : ∀ {A B   X  } → LPn A ∙ X ⊢ B → LPn X ⊢ [ A ]ᵀ ⟫ B
  ⊗I : ∀ {A B   X Y} → LPn X ⊢ A → LPn Y ⊢ B → LPn X ∙ Y ⊢ A ⊗ B

  assᵣ : ∀ {Y Z W D XR XL}
    {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
    {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
    (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
    → LPn XL ⊢ D → LPn XR ⊢ D
  assₗ : ∀ {Y Z W D XR XL}
    {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
    {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
    (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
    → LPn XR ⊢ D → LPn XL ⊢ D

  comm : ∀ {Y Z C}{X₁ X₂}{s1 : (Z ∙ Y) ∈s X₁}{s2 : (Y ∙ Z) ∈s X₂}
    (s3 : ((X₁ ─ (Z ∙ Y)) s1) ≡ ((X₂ ─ (Y ∙ Z)) s2))
    → LPn X₁ ⊢ C → LPn X₂ ⊢ C
\end{code}

\section{SC (structural control)}
\label{sec:org08bef68}

\subsection{Logical Rules}
\label{sec:orgeed9319}
\textbf{SC}=\textbf{NL}+\(\Diamond E,\Diamond I,\Box E,\Box I,R1,R2\)

\[\addtolength{\inferLineSkip}{1pt}
\infer[\Box E]{\nd{\langle X \rangle}{A}}{\nd{X}{\Box A}}
\qquad
\infer[\Box I]{\nd{X}{\Box A}}{\nd{\langle X \rangle}{A}}
\]
\[\addtolength{\inferLineSkip}{1pt}
\infer[\Diamond E]{\nd{X[Y]}{B}}{\nd{Y}{\Diamond A} & \nd{X[\langle A \rangle]}{B}}
\qquad
\infer[\Diamond I]{\nd{\langle X \rangle}{\Diamond A}}{\nd{X}{A}}
\]

\[\addtolength{\inferLineSkip}{1pt}
\infer[R1]{X[W \cdot (Y \cdot \langle Z \rangle)] \to A}{X[(W \cdot Y) \cdot \langle Z \rangle] \to A}
\qquad
\infer[R1']{X[(\langle W \rangle \cdot Y) \cdot Z] \to A}{X[\langle W \rangle \cdot (Y \cdot Z)] \to A}
\]
\[\addtolength{\inferLineSkip}{1pt}
\infer[R2]{X[(W \cdot Y) \cdot \langle Z \rangle] \to A}{X[(W \cdot \langle Z \rangle) \cdot Y] \to A}
\qquad
\infer[R2']{X[\langle W \rangle \cdot (Y \cdot Z)] \to A}{X[Y \cdot (\langle W \rangle \cdot Z)] \to A}
\]

\subsection{Agda Rules}
\label{sec:org6e7c31b}
\begin{code}
data SCn_ : Natural → Set where
  ax : (A : Struct)  → SCn A ⊢ [ A ]ᵀ
  ⟪E : ∀ {A B   X Y} → SCn X ⊢ B ⟪ A → SCn Y ⊢ A → SCn X ∙ Y ⊢ B
  ⟫E : ∀ {A B   X Y} → SCn X ⊢ A → SCn Y ⊢ A ⟫ B → SCn X ∙ Y ⊢ B
  ⊗E : ∀ {A B C   Y}{X[A∙B] X[Y]}{s1 : (A ∙ B) ∈s X[A∙B]}{s2 : Y ∈s X[Y]}
    → (s3 : ((X[A∙B] ─ (A ∙ B)) s1) ≡ ((X[Y] ─ Y) s2))
    → SCn Y ⊢ [ A ]ᵀ ⊗ [ B ]ᵀ → SCn X[A∙B] ⊢ C → SCn X[Y] ⊢ C
  ◇E : ∀ {A B     Y}{X[⟨A⟩] X[Y]}{s1 : ⟨ A ⟩ ∈s X[⟨A⟩]}{s2 : Y ∈s X[Y]}
    → (s3 : ((X[⟨A⟩] ─ ⟨ A ⟩) s1) ≡ ((X[Y] ─ Y) s2))
    → SCn Y ⊢ ◇ [ A ]ᵀ → SCn X[⟨A⟩] ⊢ B → SCn X[Y] ⊢ B
  □E : ∀ {A     X  } → SCn X ⊢ □ A → SCn ⟨ X ⟩ ⊢ A
  ⟪I : ∀ {A B   X  } → SCn X ∙ A ⊢ B → SCn X ⊢ B ⟪ [ A ]ᵀ
  ⟫I : ∀ {A B   X  } → SCn A ∙ X ⊢ B → SCn X ⊢ [ A ]ᵀ ⟫ B
  ⊗I : ∀ {A B   X Y} → SCn X ⊢ A → SCn Y ⊢ B → SCn X ∙ Y ⊢ A ⊗ B
  ◇I : ∀ {A     X  } → SCn X ⊢ A → SCn ⟨ X ⟩ ⊢ ◇ A
  □I : ∀ {A     X  } → SCn ⟨ X ⟩ ⊢ A → SCn X ⊢ □ A

  R1  : ∀ {A W Y Z XL XR}
    {s1 : (W ∙ (Y ∙ ⟨ Z ⟩)) ∈s XR}
    {s2 : ((W ∙ Y) ∙ ⟨ Z ⟩) ∈s XL}
    (s3 : ((XR ─ (W ∙ (Y ∙ ⟨ Z ⟩))) s1) ≡ ((XL ─ ((W ∙ Y) ∙ ⟨ Z ⟩)) s2))
    → SCn XR ⊢ A → SCn XL ⊢ A
  R1` : ∀ {A W Y Z XL XR}
    {s1 : (⟨ W ⟩ ∙ (Y ∙ Z)) ∈s XR}
    {s2 : ((⟨ W ⟩ ∙ Y) ∙ Z) ∈s XL}
    (s3 : ((XR ─ (⟨ W ⟩ ∙ (Y ∙ Z))) s1) ≡ ((XL ─ ((⟨ W ⟩ ∙ Y) ∙ Z)) s2))
    → SCn XL ⊢ A → SCn XR ⊢ A
  R2  : ∀ {A W Y Z XL XR}
    {s1 : (W ∙ (Y ∙ ⟨ Z ⟩)) ∈s XR}
    {s2 : ((W ∙ ⟨ Z ⟩) ∙ Y) ∈s XL}
    (s3 : ((XR ─ (W ∙ (Y ∙ ⟨ Z ⟩))) s1) ≡ ((XL ─ ((W ∙ ⟨ Z ⟩) ∙ Y)) s2))
    → SCn XL ⊢ A → SCn XR ⊢ A
  R2` : ∀ {A W Y Z XL XR}
    {s1 : (⟨ W ⟩ ∙ (Y ∙ Z)) ∈s XR}
    {s2 : ((Y ∙ ⟨ W ⟩) ∙ Z) ∈s XL}
    (s3 : ((XR ─ (⟨ W ⟩ ∙ (Y ∙ Z))) s1) ≡ ((XL ─ ((Y ∙ ⟨ W ⟩) ∙ Z)) s2))
    → SCn XL ⊢ A → SCn XR ⊢ A
\end{code}

\section{L (associativity implicit)}
\label{sec:org3887721}

We can define a version of \texttt{L} where associativity rules are implicitly applied in a proof by using a different structure and corresponding functions for a sequent. Where we used \texttt{Struct → Type → Natural} as the type for a sequent before, we now use \texttt{List → Type → Natural}. (Note that this list is not a polymorphic list, but strictly a list of \texttt{Type}.) Because the concatention function \texttt{++} is inherently associative, this structure allows us to omit associative rules from any proof.

\begin{code}
data Natural` : Set where
  _⊢_ : List → Type → Natural`
\end{code}

\subsection{Logical Rules}
\label{sec:orgeab8feb}
\[\addtolength{\inferLineSkip}{1pt}
\infer[\bs I]{\nd{X}{A\bs B}}{\nd{A , X}{B}}
\qquad
\infer[\bs E]{\nd{X , Y}{B}}{\nd{X}{A} & \nd{Y}{A\bs B}}
\]
%
\[\addtolength{\inferLineSkip}{1pt}
\infer[\slash I]{\nd{X}{B \fs A}}{\nd{X , A}{B}}
\qquad
\infer[\slash E]{\nd{X , Y}{B}}{\nd{X}{B \fs A} & \nd{Y}{A}}
\]
%
\[\addtolength{\inferLineSkip}{1pt}
\infer[\otimes I]{\nd{X , Y}{A \otimes B}}{\nd{X}{A} & \nd{Y}{B}}
\qquad
\infer[\otimes E]{\nd{Y , X , Y'}{C}}{\nd{Y}{A \otimes B} & \nd{X[A · B]}{C}}
\]

\subsection{Agda Rules}
\label{sec:orgb484f95}
\begin{code}
infix 4 Ln`_
data Ln`_ : Natural` → Set where
  ax : {X : List} → (A : Type) → A ∈l X → Ln` X ⊢ A
  ⟫E : ∀ {A B   X Y  } → Ln` X ⊢ A → Ln` Y ⊢ A ⟫ B → Ln` X ++ Y ⊢ B
  ⟪E : ∀ {A B   X Y  } → Ln` X ⊢ B ⟪ A → Ln` Y ⊢ A → Ln` X ++ Y ⊢ B
  ⊗E : ∀ {A B C X Y Z} → Ln` X ⊢ A ⊗ B → Ln` Y ++ (A ∷ ø ++ (B ∷ ø ++ Z)) ⊢ C
    → Ln` (X ++ Y) ++ Z ⊢ C
  ⟫I : ∀ {A B   X    } → Ln` A ∷ ø ++ X ⊢ B → Ln` X ⊢ A ⟫ B
  ⟪I : ∀ {A B   X    } → Ln` X ++ A ∷ ø ⊢ B → Ln` X ⊢ B ⟪ A
  ⊗I : ∀ {A B   X Y  } → Ln` X ⊢ A → Ln` Y ⊢ B → Ln` X ++ Y ⊢ A ⊗ B
\end{code}

\section{LP (commutativity implicit)}
\label{sec:org55cf5da}

\begin{code}
data Natural`` : Set where
  _⊢_ : Multiset → Type → Natural``
\end{code}

Similar to the approach for implicit associativity, we can model implicit commutativity by using a multiset instead of a tree as structure. The multiset-operators \texttt{∪}, \texttt{⊂} and \texttt{⊆}, defined in the \texttt{Logic} module are inherently associative, so by using these operators, we can omit both the associativity rules and the commutativity rules. An effect of implicit commutativity is that directed application collapses, so instead of \texttt{A ⟫ B} and \texttt{B ⟪ A} we just have \texttt{A ⊸ B}.

\subsection{Logical Rules}
\label{sec:orgaed9585}

\newcommand{\lo}{\multimap}
\[\addtolength{\inferLineSkip}{1pt}
\infer[\lo I]{X \to A \lo B}{X , A \to B}
\qquad
\infer[\lo E]{X , Y \to B}{X \to A \lo B & Y \to A}
\]
\[\addtolength{\inferLineSkip}{1pt}
\infer[\otimes I]{X , Y \to A \otimes B}{X \to A & Y \to B}
\qquad
\infer[\otimes E]{X , Y \to C}{Y \to A \otimes B & X , A , B \to C}
\]

\subsection{Agda Rules}
\label{sec:org0e565c2}
\begin{code}
infix 4 LPn`_
data LPn`_ : Natural`` → Set where
  ax : {X : Multiset}  → (A : Type) → A ∈m X → LPn` X ⊢ A
  ⊸E : ∀ {A B   X Y Z} → (p : (X ∪ Y) ⊂ Z)
     → LPn` X ⊢ A ⊸ B → LPn` Y ⊢ A → LPn` Z ⊢ B
  ⊗E : ∀ {A B C X X` Y Z} → (q : (A , (B , X)) ⊂ X`) → (p : (X ∪ Y) ⊂ Z)
     → LPn` Y ⊢ A ⊗ B → LPn` X` ⊢ C → LPn` Z ⊢ C
  ⊸I : ∀ {A B   X X` } → (p : (A , X`) ⊂ X)
     → LPn` X ⊢ B → LPn` X` ⊢ A ⊸ B
  ⊗I : ∀ {A B   X Y Z} → (p : (X ∪ Y) ⊂ Z)
     → LPn` X ⊢ A → LPn` Y ⊢ B → LPn` Z ⊢ A ⊗ B
\end{code}

The fact that \texttt{LP`} does indeed handle commutativity implicitly might not be obvious. We can therefore prove it by assuming that \texttt{Γ} is a reordering of \texttt{Γ`} (we have defined \texttt{A ⊆ B} to mean both \texttt{A ⊂ B} and \texttt{B ⊂ A}.) and proving that for every NDProof \texttt{Γ ⊢ A} there is a corresponding proof for \texttt{Γ` ⊢ A}.
\begin{tutorial}
First note that elements like \texttt{ABX⊂X} are variable names, I have chosen them to make the proof slightly more readable. Secondly, the proof utilizes quite some functions from the Logic module to make the proof more succint. Lastly, to construct this proof, it is very benificial to use the `holes' that Agda provides. For any element in the proof, one can insert a `?', which Agda will interpret as a hole, and it can provide the current variables that are in scope and their types and the goal for the current hole. This way the proof is constructed incrementally, and it is obvious where sub-proofs such as \texttt{⊂-trans} should occur.
\end{tutorial}

\begin{code}
Π-comm : ∀ {A Γ Γ`} → Γ ⊆ Γ` → LPn` Γ ⊢ A → LPn` Γ` ⊢ A
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (ax     A    A∈Γ  )
  = ax A (Γ⊂Γ` A∈Γ)
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (⊸E  XY⊂Γ    π ρ)
  = ⊸E (⊂-trans XY⊂Γ Γ⊂Γ`) (Π-comm ⊆-refl π) (Π-comm ⊆-refl ρ)
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (⊗E ABX⊂X XY⊂Γ π ρ)
  = ⊗E ABX⊂X (⊂-trans XY⊂Γ Γ⊂Γ`) (Π-comm ⊆-refl π) (Π-comm ⊆-refl ρ)
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (⊸I  AΓ⊂X    π  )
  = ⊸I (⊂-trans (⊂-weakⁱ Γ`⊂Γ) AΓ⊂X) (Π-comm ⊆-refl π)
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (⊗I  XY⊂Γ    π ρ)
  = ⊗I (⊂-trans XY⊂Γ Γ⊂Γ`) (Π-comm ⊆-refl π) (Π-comm ⊆-refl ρ)
\end{code}
\end{document}