% Created 2019-05-06 ma 13:52
% Intended LaTeX compiler: xelatex
\documentclass[a4paper, 11pt]{article}
    \input{/home/bram/.dotfiles/latex/agda.tex}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usetag{tutorial,comment,note}
\author{Bram van den Boomen}
\date{\today}
\title{Logic Module}
\hypersetup{
 pdfauthor={Bram van den Boomen},
 pdftitle={Logic Module},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.2 (Org mode 9.1.9)}, 
 pdflang={English}}
\usepackage[conor]{agda}

%include lhs2TeX.fmt
\begin{document}

\maketitle

\section{Preface}
\label{sec:org719d5a3}
\emph{Version: Main \tagged{tutorial}{+ Tutorial} \tagged{note}{+ Notes}}

For this project I have chosen to import very few libraries, mainly to help my own learning process of how Agda works with Lambek-calculi. Nevertheless, some libraries would be silly to re-implement, so I import them here.

\begin{code}
open import Relation.Binary.PropositionalEquality
  using (_≡_; refl; cong; subst; sym)
open import Relation.Nullary using (¬_)
open import Data.String using (String)
open import Data.Bool
open import Data.Empty using (⊥; ⊥-elim)
\end{code}

\begin{tutorial}
All Agda files should have a top-level module name which matches the file name. This module therefore corresponds to Logic.agda (or in the case of literate Agda files ``Logic.lagda.tex'' or ``Logic.lagda.org'').
\end{tutorial}
\begin{code}
module Logic where
\end{code}

\section{Types}
\label{sec:org09a2185}

The \texttt{Type} type is the inductive definition of all possible types. Not all calculi will use all operators, for instance, non-commutative natural deduction will not use the \texttt{◇ □ ⊸} operators. Because \texttt{\textbackslash{}} and \texttt{/}, the characters for directed function application, are reserved characters in Agda, I have opted to use \texttt{⟫} to replace \texttt{\textbackslash{}} and \texttt{⟪} to replace \texttt{/}.
\begin{note}
This might be problematic when needing to pattern-match on Types in a specific calculus. In that case we would need to specify multiple Type-classes for separate calculi.
\end{note}
The choice for the \texttt{e} type to be \texttt{String → Type} is largely arbitrary for now. It allows us to make an infinite number of atomic types when needed. Because this would not read very nicely, I have postulated that \texttt{np}, \texttt{s} and \texttt{n} are Types.

\begin{code}
-- Defining the logical operators
-- Using A ⟫ B for A\B and A ⟪ B for A / B
infix 8 _⟪_ _⟫_ _⊸_ _⊗_
infix 9 e ◇_ □_
data Type : Set where
  e    : String → Type
  _⟪_  : Type → Type → Type
  _⟫_  : Type → Type → Type
  _⊸_  : Type → Type → Type
  _⊗_  : Type → Type → Type
  ◇_   : Type → Type
  □_   : Type → Type

\end{code}

\begin{tutorial}
A postulate allows to give a type-assignment to a variable without specifying a function for that variable.
\end{tutorial}
\begin{code}
postulate
  np s n : Type
\end{code}

\section{Structures}
\label{sec:orgf6e6fd2}
\subsection{Tree}
\label{sec:org8e8b810}

The main structure for the Lambek-calculus will be the \texttt{Struct} Type, which is a binary tree in the normal calculi (using only the \texttt{⌈\_⌉} and \texttt{\_∙\_} operators) but extended to a unary/binary tree when structural control (\texttt{◇\_} and \texttt{□\_}) is added. The Type of \texttt{øStruct} is added to provide a case for when a structure is empty. When we define the \texttt{Struct ─ Struct} operation which prunes substructures from their parent-structure later on, we will need a case for \texttt{A ─ A}, which should be empty. Another way to go would be to include \texttt{ø} in \texttt{Struct} and define a data-type for non-empty structures. This type is included for completeness only. In this case, this is not desirable, because it would leave us with having to prove for every translation from structures to types that the structure is not empty, which is not very elegant providing we only need the empty structure for the prune-operation. We do of course need a translation \texttt{Struct → øStruct}.

\begin{code}
infix 7 _∙_
infix 8 ⌈_⌉
data Struct : Set where
  ⌈_⌉ : Type → Struct
  _∙_ : Struct → Struct → Struct
  ⟨_⟩ : Struct → Struct

data øStruct : Set where
  ø   : øStruct
  ⌈_⌉ : Type → øStruct
  _∙_ : øStruct → øStruct → øStruct
  ⟨_⟩ : øStruct → øStruct

[_]⁰ : Struct → øStruct
[ ⌈ x ⌉ ]⁰ = ⌈ x ⌉
[ (s₁ ∙ s₂) ]⁰ = ([ s₁ ]⁰ ∙ [ s₂ ]⁰)
[ ⟨ s₁ ⟩ ]⁰ = ⟨ [ s₁ ]⁰ ⟩

data øStruct⁺ : Struct → Set where
  leaf   : ∀ {A : Type → øStruct}{t : Type} → øStruct⁺ (⌈ t ⌉)
  binary : ∀ {A B} → øStruct⁺ A → øStruct⁺ B → øStruct⁺ (A ∙ B)
  unary  : ∀ {A} → øStruct⁺ A → øStruct⁺ (⟨ A ⟩)
\end{code}

When dealing with Natural Deduction and Sequent Calculus, there is a notion of a substructure. We therefore need to define what it means for a structure to be part of another structure. We have three definitions here which are equivalent: \texttt{X [ A ]}, \texttt{[ A ] X} and \texttt{A ∈s X} all mean that there is a proof that A is a substructure of X. (The \texttt{∈s} operation is just for consistency and readability.)
\begin{tutorial}
When defining datatypes, we can provide parameters and indices to this type. Looking at the definition of \texttt{\_[\_]}, this type is indexed over two instances of \texttt{Struct}. The second definition (\texttt{[\_]\_}) has a named parameter A and is indexed over one \texttt{Struct}. The main difference is that parameters need to be the same for all constructors of the type, while indices can vary between constructors.
\end{tutorial}

\begin{note}
Currently I am using the second definition of substructure on the assumption that it is simpler than the first. But there might be need to check if this impacts performance, particularly during proof search.
\end{note}
\begin{code}
infix 6 _[_] [_]_
data _[_] : Struct → Struct → Set where
  here  : {A : Struct} → A [ A ]
  left  : {A X Y : Struct} → X [ A ] → (X ∙ Y) [ A ]
  right : {A X Y : Struct} → X [ A ] → (Y ∙ X) [ A ]

data [_]_ (A : Struct) : Struct → Set where
  here  : [ A ] A
  left  : ∀ {X Y : Struct} → [ A ] X → [ A ] (X ∙ Y)
  right : ∀ {X Y : Struct} → [ A ] Y → [ A ] (X ∙ Y)

_∈s_ : Struct → Struct → Set
A ∈s X = [ A ] X
\end{code}

As mentioned, we need an operation that takes a substructure from its parent structure and returns the remaining structure. This is useful when formalizing logical rules in the form of \texttt{X[A] → X[Y]} Which means `if structure \texttt{X} has substructure \texttt{A}, then we can substitute \texttt{A} for substructure \texttt{Y}'. To be sure that this only returns an empty structure when subtracting a structure from itself, the definition is slightly more involved than would be expected. This is necessary to prevent the function returning a structure such as \texttt{(ø ∙ A)}.

\begin{tutorial}
This is where dependent types come in handy. In a normal definition we would have to define a case for when B is not a substructure of A, so there is nothing to remove. However, in our logic, that should and will never be the case. We therefore give a third argument to the function, which not only gives a proof \textit{that} B is a substructure of A, but as an added bonus also \textit{where} this substructure is located. This way, the function cannot be called without proof that B is indeed a substructure of A. Also we do not need to concern ourselves with pattern-matching on the equality of entire structures (for which we would need to define a relation which defines what it means for two substructures to be equal in the first place).
\end{tutorial}

\begin{code}
_─_ : (A : Struct) → (B : Struct) → B ∈s A → øStruct
(A ─ A) here = ø
((L ∙ R) ─ .L) (left here) = [ R ]⁰
((L ∙ R) ─ .R) (right here) = [ L ]⁰
((L ∙ R) ─ A) (left (left P)) = ((L ─ A) (left P)) ∙ [ R ]⁰
((L ∙ R) ─ A) (left (right P)) = ((L ─ A) (right P)) ∙ [ R ]⁰
((L ∙ R) ─ A) (right (left P)) = [ L ]⁰ ∙ ((R ─ A) (left P))
((L ∙ R) ─ A) (right (right P)) = [ L ]⁰ ∙ ((R ─ A) (right P))
\end{code}

Because natural deduction has sequents of the type \texttt{Struct ⊢ Type}, we will need to be able to translate from \texttt{Struct} to \texttt{Type}, for instance to define axioms. The axiom for natural deduction is \texttt{A ⊢ A}, but \texttt{A} cannot be a \texttt{Struct} and a \texttt{Type} simultaneously.
\begin{note}
If we would like to prove that Combinatorical NP and Natural Deduction NP are equal, we would need to be able to prove that \texttt{[ [ A ]ˢ ]ᵗ ≡ A} but this is not possible. An alternative way could be to redefine the Categorial sequent as \texttt{Struct ⊢ Type} as well.
\end{note}

\begin{code}
infix 6 [_]ˢ [_]ᵀ
[_]ˢ : Type → Struct
[ e x ]ˢ = ⌈ e x ⌉
[ A ⟪ B ]ˢ = ⌈ A ⟪ B ⌉
[ A ⟫ B ]ˢ = ⌈ A ⟫ B ⌉
[ A ⊸ B ]ˢ = ⌈ A ⊸ B ⌉
[ A ⊗ B ]ˢ = [ A ]ˢ ∙ [ B ]ˢ
[ ◇ A ]ˢ = ⟨ [ A ]ˢ ⟩
[ □ A ]ˢ = [ A ]ˢ

[_]ᵀ : Struct → Type
[ ⌈ x ⌉ ]ᵀ = x
[ ⟨ x ⟩ ]ᵀ = ◇ [ x ]ᵀ
[ x₁ ∙ x₂ ]ᵀ = [ x₁ ]ᵀ ⊗ [ x₂ ]ᵀ
\end{code}

\subsection{List}
\label{sec:org305bf11}

We define \texttt{List} as a structure for inference with implicit associativity. This definition is not very different from the \texttt{List} definition in the standard library, except that this list is not polymorphic; it is always a list of \texttt{Type}. As concatenation will be our main connective with the inference rules, we should prove that concatenation respects associativity, which we can do by proving that for all \texttt{A}, \texttt{B} and \texttt{C} of type \texttt{List}, \texttt{A ++ (B ++ C)} is equal to \texttt{(A ++ B) ++ C}.
\begin{tutorial}
There is some syntactic sugar below:
The \texttt{∀} glyph can (almost) always be omitted. I like to be explicit when proving properties, that the arguments to these properties are universally quantified. Also, \texttt{(A B C : List)} could be rewritten as \texttt{(A : List)(B : List)(C : List)} or even more explicitly as \texttt{(A : List) → (B : List) → (C : List)}. Finally, something interesting is happening with the mixfix operator in the final line of \texttt{++-ass}. We can define a mixfix operator with underscores, where the underscores denote where the arguments to the function go. For example, if we have the function \texttt{\_∷\_ : Type → List → List}, \texttt{A : List} and \texttt{t : Type}, then \texttt{t ∷ A} would append \texttt{t} to \texttt{A}. In this case we would like to pass the function with only the \texttt{t} as argument, so returning the \texttt{List → List} function. Which we could do either as \texttt{(t ∷\_)} or \texttt{(\_∷\_ t)}.
\end{tutorial}

\begin{code}
infix 7 _∷_
data List : Set where
  ø   : List
  _∷_ : Type → List → List

infix 6 _++_
_++_ : List → List → List
ø ++ B = B
(x ∷ A) ++ B =  x ∷ (A ++ B)

data _∈l_ (A : Type) : (B : List) → Set where
  here  : ∀ {X : List} → A ∈l (A ∷ X)
  there : ∀ {A₁ : Type}{X : List} → A ∈l X → A ∈l (A₁ ∷ X)

-- Prove associativity
++-ass : ∀ (A B C : List) → A ++ (B ++ C) ≡ (A ++ B) ++ C
++-ass ø B C = refl
++-ass (x ∷ A) B C = cong (x ∷_) (++-ass A B C)
\end{code}

\subsection{Multiset}
\label{sec:orgfe9f563}

Just as with \texttt{List} we want to have an alternative to \texttt{Struct} that is implicitly associative, but commutative as well. For this, classically a multiset is used. A multiset is a set of records which have an element field and a corresponding cardinality. Because this would introduce some confusing complexity, I have implemented the multiset as a List, with some operators which should respect commutativity. To assert that these definitions respect the properties necessary for implicit associativity and commutativity we can prove some other properties, mainly to simplify the ultimate proof of commutativity of the \texttt{∈m} and \texttt{⊆} operators.

\begin{tutorial}
The \texttt{∪-id} propery seems trivial, but while Agda effortlessly unifies ‌\texttt{ø ∪ B} and \texttt{B} because of the definition in \texttt{\_∪\_} it does not do so for \texttt{B ∪ ø} and \texttt{B}. By proving the \texttt{∪-idʳ} property, we can use the \texttt{subst} and \texttt{sym} function from the \texttt{PropositionalEquality} library to substitute \texttt{B ∪ ø} for \texttt{B} where necessary.
\end{tutorial}

\begin{code}
data Multiset : Set where
  ø    : Multiset
  _,_  : Type → Multiset → Multiset

_∪_ : Multiset → Multiset → Multiset
ø ∪ B = B
(x , A) ∪ B = x , (A ∪ B)

∪-idˡ : ∀ {B : Multiset} → B ≡ (ø ∪ B)
∪-idˡ = refl
∪-idʳ : ∀ {B : Multiset} → B ≡ (B ∪ ø)
∪-idʳ {ø} = refl
∪-idʳ {x , B} = cong (x ,_) ∪-idʳ

∪-ass : ∀ (A B C : Multiset) → A ∪ (B ∪ C) ≡ (A ∪ B) ∪ C
∪-ass ø B C = refl
∪-ass (x , A) B C = cong (x ,_) (∪-ass A B C)
\end{code}

The \texttt{∈m} property is very similar to the \texttt{∈s} property, except that we are looking for a single element of the list instead of a sub-list. The \texttt{∈m} proof is essentially a pointer as to where the element is.
\begin{tutorial}
As an example: if we have ‌\texttt{(a b c d : Type)} and \texttt{(M : (a , (b , (c , (d , ø))))} then \texttt{c ∈m M} would be \texttt{there (there (here))} corresponding essentially with the natural number 2 (or the third element of the list).
\end{tutorial}

\begin{code}
data _∈m_ (A : Type) : (B : Multiset) → Set where
  here  : ∀ {X : Multiset} → A ∈m (A , X)
  there : ∀ {x : Type}{X : Multiset} → A ∈m X → A ∈m (x , X)

_∉_ : (A : Type) → (B : Multiset) → Set
A ∉ X = ¬ (A ∈m X)

∈-eq : {t : Type}{A B : Multiset} → t ∈m A → A ≡ B → t ∈m B
∈-eq {t} {A} {B} x y = subst (t ∈m_) y x
∈-idˡ : {t : Type}{A : Multiset} → t ∈m A → t ∈m (ø ∪ A)
∈-idˡ A = A
∈-idᴸ : {t : Type}{A : Multiset} → t ∈m (ø ∪ A) → t ∈m A
∈-idᴸ A = A
∈-idʳ : {t : Type}{A : Multiset} → t ∈m A → t ∈m (A ∪ ø)
∈-idʳ B = ∈-eq B ∪-idʳ
∈-idᴿ : {t : Type}{A : Multiset} → t ∈m (A ∪ ø) → t ∈m A
∈-idᴿ B = ∈-eq B (sym ∪-idʳ)
\end{code}

This of course introduces a strict ordering of elements, so we need an operator for which the order of elements is irrelevant. This will be the main operator for implicit commutativity. Abusing notation slightly, the \texttt{⊂} operator is the definition of a strict subset: For \texttt{(A B : Multiset)}, \texttt{A ⊂ B} if all elements of \texttt{A} are also an element of \texttt{B}. The \texttt{⊆} operator is the definition of a reordering of a \texttt{Multiset}: \texttt{(A B : Multiset)}, \texttt{A ⊆ B} if \texttt{A ⊂ B} and \texttt{B ⊂ A}.
\begin{tutorial}
There are multiple ways in which \texttt{⊆} and \texttt{⊂} could have been implemented. The first thing that comes to mind is a new data type. However, most of these approaches tend not to deal well with the fact that a multiset is ordered. The current implementation of \texttt{⊂} abstracts over this order and is just a function from \texttt{∈m} to \texttt{∈m} or a relation between multisets. This leaves the problem of ordering up to a property-proof itself. The implementation of \texttt{⊆} uses the built-in Agda record, which is pretty straightforward.
\end{tutorial}

By omitting the cardinality of the elements we introduce an unexpected property for the \texttt{⊂} relation. In the current implementation \texttt{np , np ⊂ np} is valid because we can define a function \texttt{t ∈m A → t ∈m B} which would give \texttt{here} for both the first and second element. For our purposes however, this should not prove to be a problem, because the logic that we are using the multiset for includes weakening and contraction, which are included by this mechanism. We do however only have weakening for the same element, so we need to provide an extra definition for it at the inference-level. Because \texttt{A , A ⊆ A} the multiset by itself accounts for the following rules:

\[\addtolength{\inferLineSkip}{1pt}
\infer[weakening`]{A , A → B}{A → B} \qquad
\infer[contraction]{A → B}{A , A → B}
\]

\begin{code}
_⊂_ : Multiset → Multiset → Set
A ⊂ B = ∀ {t : Type} → t ∈m A → t ∈m B

record _⊆_ (A B : Multiset) : Set where
  constructor _&_
  field
    fst : A ⊂ B
    snd : B ⊂ A

⊂-refl : ∀ {A : Multiset} → A ⊂ A
⊂-refl x = x

⊆-refl : ∀ {A : Multiset} → A ⊆ A
⊆-refl = ⊂-refl & ⊂-refl
\end{code}

\begin{tutorial}
Remember that \texttt{A ⊂ B} essentially means \texttt{t ∈m A → t ∈m B} `if t is in A, then t is in B' so the expression below: \texttt{B⊂C (A⊂B t∈A)} can be translated as \texttt{(t ∈m B → t ∈m C) ((t ∈m A → t ∈m B) t ∈m A)} which results in \texttt{t ∈m C} The application of \texttt{⊂-trans} to \texttt{A⊂B} \texttt{B⊂C} and \texttt{t∈mA} then would return \texttt{t∈mC} so the application of \texttt{⊂-trans} to only \texttt{A⊂B} and \texttt{B⊂C} would return a function \texttt{t ∈m A → t ∈m C} which is the exact definition of \texttt{A ⊂ C}
\end{tutorial}

\begin{code}
⊂-trans : ∀ {A B C : Multiset} → A ⊂ B → B ⊂ C → A ⊂ C
⊂-trans A⊂B B⊂C t∈A = B⊂C (A⊂B t∈A)
\end{code}

The weakening proofs are not strictly necessary while most of them seem trivial at best, but they help to keep the proofs further on readable and simple.
\begin{tutorial}
In Agda, arguments between \texttt{\{ \}}- brackets are `implicit' arguments to a function. Even though these arguments are not explicitly passed to a function, we can still pattern-match on them, by including them before the explicit arguments between the same brackets. Furthermore, we can even provide them as explicit arguments to a function in the same way.
\end{tutorial}

\begin{code}
∈-weakˡ : {t : Type}{A B : Multiset} → t ∈m A → t ∈m (B ∪ A)
∈-weakˡ {t} {A} {ø} p = p
∈-weakˡ {t} {A} {_ , B} p = there (∈-weakˡ p)

∈-weakʳ : {t : Type}{A B : Multiset} → t ∈m A → t ∈m (A ∪ B)
∈-weakʳ here = here
∈-weakʳ (there p) = there (∈-weakʳ p)

∈-weakᵐ : ∀ {t x : Type}(A B : Multiset) → t ∈m (A ∪ B) → t ∈m (A ∪ (x , B))
∈-weakᵐ ø B x = there x
∈-weakᵐ (a , A) B here = here
∈-weakᵐ (a , A) B (there x) = there (∈-weakᵐ A B x)

⊂-weakʳ : ∀ {A B C : Multiset} → A ⊂ B → A ⊂ (B ∪ C)
⊂-weakʳ A⊂B t∈A = ∈-weakʳ (A⊂B t∈A)

⊂-weakˡ : ∀ {A B C : Multiset} → A ⊂ B → A ⊂ (C ∪ B)
⊂-weakˡ A⊂B t∈A = ∈-weakˡ (A⊂B t∈A)

⊂-weakⁱ : ∀ {t : Type}{A B : Multiset} → A ⊂ B → (t , A) ⊂ (t , B)
⊂-weakⁱ A⊂B here = here
⊂-weakⁱ A⊂B (there t∈A+) = there (A⊂B t∈A+)
\end{code}

Finally, we can provide some non-trivial proofs for the subset relation for multisets. The \texttt{∈-comm} and \texttt{⊂-comm} proofs are identical, except for the explicit multiset arguments in \texttt{∈-comm}. This is to be expected as \texttt{⊂} is just a function over \texttt{∈}.

\begin{code}
∪-minˡ  : ∀ {A B C : Multiset} → (A ∪ B) ⊂ C → A ⊂ C
∪-minˡ  A∪B⊂C t∈A = A∪B⊂C (∈-weakʳ t∈A)
∪-minʳ  : ∀ {A B C : Multiset} → (A ∪ B) ⊂ C → B ⊂ C
∪-minʳ  A∪B⊂C t∈A = A∪B⊂C (∈-weakˡ t∈A)

∈-comm : ∀ {t : Type}(A B : Multiset) → t ∈m (A ∪ B) → t ∈m (B ∪ A)
∈-comm ø B t∈A∪B = ∈-idʳ t∈A∪B
∈-comm (_ , As) B here = ∈-weakˡ here
∈-comm (_ , As) B (there t∈A∪B) = ∈-weakᵐ B As (∈-comm As B t∈A∪B)

⊂-comm : ∀ {A B : Multiset} → (A ∪ B) ⊂ (B ∪ A)
⊂-comm {ø} {B} t∈A∪B = ∈-idʳ t∈A∪B
⊂-comm {_ , As} {B} here = ∈-weakˡ here
⊂-comm {_ , As} {B} (there t∈A∪B) = ∈-weakᵐ B As (⊂-comm {As} {B} t∈A∪B)
\end{code}

\section{Examples}
\label{sec:orge5d56df}

To provide some insight into how some of these functions work, I have added some examples below. These will not be compiled by Agda when running this document!

\begin{code}
-- Examples
_ : ∀ {A B C : Struct} → ((A ∙ C) ─ A) (left here) ≡ ((C ∙ B) ─ B) (right here)
_ = refl

_ : (np , (s , ø)) ⊂ (s , (np , ø))
_ = ∈-comm (np , ø) (s , ø) -- (np,ø)∪(s,ø) ⊂ (s,ø)∪(np,ø)

_ : (np , (s , ø)) ⊆ (s , (np , ø))
_ = (∈-comm (np , ø) (s , ø)) & (∈-comm (s , ø) (np , ø))
\end{code}
\end{document}
