open import Agda.Builtin.Equality using (_≡_; refl)

module SequentProof where

open import Logic

infix 4 NLs_ Ls_ LPs_ SCs_
infix 5 _⇒_
data Sequent : Set where
  _⇒_ : Struct → Type → Sequent

data NLs_ : Sequent → Set where
  ax : (A : Struct) → NLs A ⇒ [ A ]ᵀ
  ⟫L : ∀ {A B C   Y}{X[B] X[Y∙A⟫B]}
           {s1 : ⌈ B ⌉ ∈s X[B]}
           {s2 : (Y ∙ ⌈ A ⟫ B ⌉) ∈s X[Y∙A⟫B]}
           (s3 : ((X[B] ─ ⌈ B ⌉) s1) ≡ ((X[Y∙A⟫B] ─ (Y ∙ ⌈ A ⟫ B ⌉)) s2))
                     → NLs Y ⇒ A → NLs X[B] ⇒ C → NLs X[Y∙A⟫B] ⇒ C
  ⟫R : ∀ {A B   X  } → NLs ⌈ A ⌉ ∙ X ⇒ B → NLs X ⇒ A ⟫ B
  ⟪L : ∀ {A B C   Y}{X[B] X[B⟪A∙Y]}
           {s1 : ⌈ B ⌉ ∈s X[B]}
           {s2 : (⌈ A ⟪ B ⌉ ∙ Y) ∈s X[B⟪A∙Y]}
           (s3 : ((X[B] ─ ⌈ B ⌉) s1) ≡ ((X[B⟪A∙Y] ─ (⌈ A ⟪ B ⌉ ∙ Y)) s2))
                     → NLs Y ⇒ A → NLs X[B] ⇒ C → NLs X[B⟪A∙Y] ⇒ C
  ⟪R : ∀ {A B   X  } → NLs X ∙ ⌈ A ⌉ ⇒ B → NLs X ⇒ B ⟪ A
  ⊗L : ∀ {A B C    }{X[A∙B] X[A⊗B]}
           {s1 : (⌈ A ⌉ ∙ ⌈ B ⌉) ∈s X[A∙B]}
           {s2 : ⌈ A ⊗ B ⌉ ∈s X[A⊗B]}
           (s3 : ((X[A∙B] ─ (⌈ A ⌉ ∙ ⌈ B ⌉)) s1) ≡ ((X[A⊗B] ─ ⌈ A ⊗ B ⌉) s2))
                     → NLs X[A∙B] ⇒ C → NLs X[A⊗B] ⇒ C
  ⊗R : ∀ {A B   X Y} → NLs X ⇒ A → NLs Y ⇒ B → NLs X ∙ Y ⇒ A ⊗ B

data Ls_ : Sequent → Set where
  ax : (A : Struct) → Ls A ⇒ [ A ]ᵀ
  ⟫L : ∀ {A B C   Y}{X[B] X[Y∙A⟫B]}
           {s1 : ⌈ B ⌉ ∈s X[B]}
           {s2 : (Y ∙ ⌈ A ⟫ B ⌉) ∈s X[Y∙A⟫B]}
           (s3 : ((X[B] ─ ⌈ B ⌉) s1) ≡ ((X[Y∙A⟫B] ─ (Y ∙ ⌈ A ⟫ B ⌉)) s2))
                     → Ls Y ⇒ A → Ls X[B] ⇒ C → Ls X[Y∙A⟫B] ⇒ C
  ⟫R : ∀ {A B   X  } → Ls ⌈ A ⌉ ∙ X ⇒ B → Ls X ⇒ A ⟫ B
  ⟪L : ∀ {A B C   Y}{X[B] X[B⟪A∙Y]}
           {s1 : ⌈ B ⌉ ∈s X[B]}
           {s2 : (⌈ A ⟪ B ⌉ ∙ Y) ∈s X[B⟪A∙Y]}
           (s3 : ((X[B] ─ ⌈ B ⌉) s1) ≡ ((X[B⟪A∙Y] ─ (⌈ A ⟪ B ⌉ ∙ Y)) s2))
                     → Ls Y ⇒ A → Ls X[B] ⇒ C → Ls X[B⟪A∙Y] ⇒ C
  ⟪R : ∀ {A B   X  } → Ls X ∙ ⌈ A ⌉ ⇒ B → Ls X ⇒ B ⟪ A
  ⊗L : ∀ {A B C    }{X[A∙B] X[A⊗B]}
           {s1 : (⌈ A ⌉ ∙ ⌈ B ⌉) ∈s X[A∙B]}
           {s2 : ⌈ A ⊗ B ⌉ ∈s X[A⊗B]}
           (s3 : ((X[A∙B] ─ (⌈ A ⌉ ∙ ⌈ B ⌉)) s1) ≡ ((X[A⊗B] ─ ⌈ A ⊗ B ⌉) s2))
                     → Ls X[A∙B] ⇒ C → Ls X[A⊗B] ⇒ C
  ⊗R : ∀ {A B   X Y} → Ls X ⇒ A → Ls Y ⇒ B → Ls X ∙ Y ⇒ A ⊗ B

  assᵣ : ∀ {Y Z W D XR XL}
           {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
           {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
           (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
               → Ls XL ⇒ D → Ls XR ⇒ D
  assₗ : ∀ {Y Z W D XR XL}
           {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
           {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
           (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
               → Ls XR ⇒ D → Ls XL ⇒ D

data LPs_ : Sequent → Set where
  ax : (A : Struct) → LPs A ⇒ [ A ]ᵀ
  ⟫L : ∀ {A B C   Y}{X[B] X[Y∙A⟫B]}
           {s1 : ⌈ B ⌉ ∈s X[B]}
           {s2 : (Y ∙ ⌈ A ⟫ B ⌉) ∈s X[Y∙A⟫B]}
           (s3 : ((X[B] ─ ⌈ B ⌉) s1) ≡ ((X[Y∙A⟫B] ─ (Y ∙ ⌈ A ⟫ B ⌉)) s2))
                     → LPs Y ⇒ A → LPs X[B] ⇒ C → LPs X[Y∙A⟫B] ⇒ C
  ⟫R : ∀ {A B   X  } → LPs ⌈ A ⌉ ∙ X ⇒ B → LPs X ⇒ A ⟫ B
  ⟪L : ∀ {A B C   Y}{X[B] X[B⟪A∙Y]}
           {s1 : ⌈ B ⌉ ∈s X[B]}
           {s2 : (⌈ A ⟪ B ⌉ ∙ Y) ∈s X[B⟪A∙Y]}
           (s3 : ((X[B] ─ ⌈ B ⌉) s1) ≡ ((X[B⟪A∙Y] ─ (⌈ A ⟪ B ⌉ ∙ Y)) s2))
                     → LPs Y ⇒ A → LPs X[B] ⇒ C → LPs X[B⟪A∙Y] ⇒ C
  ⟪R : ∀ {A B   X  } → LPs X ∙ ⌈ A ⌉ ⇒ B → LPs X ⇒ B ⟪ A
  ⊗L : ∀ {A B C    }{X[A∙B] X[A⊗B]}
           {s1 : (⌈ A ⌉ ∙ ⌈ B ⌉) ∈s X[A∙B]}
           {s2 : ⌈ A ⊗ B ⌉ ∈s X[A⊗B]}
           (s3 : ((X[A∙B] ─ (⌈ A ⌉ ∙ ⌈ B ⌉)) s1) ≡ ((X[A⊗B] ─ ⌈ A ⊗ B ⌉) s2))
                     → LPs X[A∙B] ⇒ C → LPs X[A⊗B] ⇒ C
  ⊗R : ∀ {A B   X Y} → LPs X ⇒ A → LPs Y ⇒ B → LPs X ∙ Y ⇒ A ⊗ B

  assᵣ : ∀ {Y Z W D XR XL}
           {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
           {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
           (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
               → LPs XL ⇒ D → LPs XR ⇒ D
  assₗ : ∀ {Y Z W D XR XL}
           {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
           {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
           (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
               → LPs XR ⇒ D → LPs XL ⇒ D

  comm : ∀ {Y Z C}{X₁ X₂}
           {s1 : (Z ∙ Y) ∈s X₁}
           {s2 : (Y ∙ Z) ∈s X₂}
           (s3 : ((X₁ ─ (Z ∙ Y)) s1) ≡ ((X₂ ─ (Y ∙ Z)) s2))
               → LPs X₁ ⇒ C → LPs X₂ ⇒ C

data SCs_ : Sequent → Set where
  ax : (A : Struct) → SCs A ⇒ [ A ]ᵀ
  □L : ∀ {A B}{X[A] X[⟨□A⟩]}
           {s1 : ⌈ A ⌉ ∈s X[A]}
           {s2 : ⟨ ⌈ □ A ⌉ ⟩ ∈s X[⟨□A⟩]}
           (s3 : ((X[A] ─ ⌈ A ⌉) s1) ≡ ((X[⟨□A⟩] ─ ⟨ ⌈ □ A ⌉ ⟩) s2))
                    → SCs X[A] ⇒ B → SCs X[⟨□A⟩] ⇒ B
  □R : ∀ {A X} → SCs ⟨ X ⟩ ⇒ A → SCs X ⇒ □ A
  ◇L : ∀ {A B}{X[◇A] X[⟨A⟩]}
           {s1 : ⌈ ◇ A ⌉ ∈s X[◇A]}
           {s2 : ⟨ ⌈ A ⌉ ⟩ ∈s X[⟨A⟩]}
           (s3 : ((X[◇A] ─ ⌈ ◇ A ⌉) s1) ≡ ((X[⟨A⟩] ─ ⟨ ⌈ A ⌉ ⟩) s2))
                    → SCs X[⟨A⟩] ⇒ B → SCs X[◇A] ⇒ B
  ◇R : ∀ {A X} → SCs X ⇒ A → SCs ⟨ X ⟩ ⇒ ◇ A
  ⟫L : ∀ {A B C   Y}{X[B] X[Y∙A⟫B]}
           {s1 : ⌈ B ⌉ ∈s X[B]}
           {s2 : (Y ∙ ⌈ A ⟫ B ⌉) ∈s X[Y∙A⟫B]}
           (s3 : ((X[B] ─ ⌈ B ⌉) s1) ≡ ((X[Y∙A⟫B] ─ (Y ∙ ⌈ A ⟫ B ⌉)) s2))
                     → SCs Y ⇒ A → SCs X[B] ⇒ C → SCs X[Y∙A⟫B] ⇒ C
  ⟫R : ∀ {A B   X  } → SCs ⌈ A ⌉ ∙ X ⇒ B → SCs X ⇒ A ⟫ B
  ⟪L : ∀ {A B C   Y}{X[B] X[B⟪A∙Y]}
           {s1 : ⌈ B ⌉ ∈s X[B]}
           {s2 : (⌈ A ⟪ B ⌉ ∙ Y) ∈s X[B⟪A∙Y]}
           (s3 : ((X[B] ─ ⌈ B ⌉) s1) ≡ ((X[B⟪A∙Y] ─ (⌈ A ⟪ B ⌉ ∙ Y)) s2))
                     → SCs Y ⇒ A → SCs X[B] ⇒ C → SCs X[B⟪A∙Y] ⇒ C
  ⟪R : ∀ {A B   X  } → SCs X ∙ ⌈ A ⌉ ⇒ B → SCs X ⇒ B ⟪ A
  ⊗L : ∀ {A B C    }{X[A∙B] X[A⊗B]}
           {s1 : (⌈ A ⌉ ∙ ⌈ B ⌉) ∈s X[A∙B]}
           {s2 : ⌈ A ⊗ B ⌉ ∈s X[A⊗B]}
           (s3 : ((X[A∙B] ─ (⌈ A ⌉ ∙ ⌈ B ⌉)) s1) ≡ ((X[A⊗B] ─ ⌈ A ⊗ B ⌉) s2))
                     → SCs X[A∙B] ⇒ C → SCs X[A⊗B] ⇒ C
  ⊗R : ∀ {A B   X Y} → SCs X ⇒ A → SCs Y ⇒ B → SCs X ∙ Y ⇒ A ⊗ B

  R1  : ∀ {A W Y Z XL XR}
            {s1 : (W ∙ (Y ∙ ⟨ Z ⟩)) ∈s XR}
            {s2 : ((W ∙ Y) ∙ ⟨ Z ⟩) ∈s XL}
            (s3 : ((XR ─ (W ∙ (Y ∙ ⟨ Z ⟩))) s1) ≡ ((XL ─ ((W ∙ Y) ∙ ⟨ Z ⟩)) s2))
                     → SCs XR ⇒ A → SCs XL ⇒ A
  R1` : ∀ {A W Y Z XL XR}
            {s1 : (⟨ W ⟩ ∙ (Y ∙ Z)) ∈s XR}
            {s2 : ((⟨ W ⟩ ∙ Y) ∙ Z) ∈s XL}
            (s3 : ((XR ─ (⟨ W ⟩ ∙ (Y ∙ Z))) s1) ≡ ((XL ─ ((⟨ W ⟩ ∙ Y) ∙ Z)) s2))
                     → SCs XL ⇒ A → SCs XR ⇒ A
  R2  : ∀ {A W Y Z XL XR}
            {s1 : (W ∙ (Y ∙ ⟨ Z ⟩)) ∈s XR}
            {s2 : ((W ∙ ⟨ Z ⟩) ∙ Y) ∈s XL}
            (s3 : ((XR ─ (W ∙ (Y ∙ ⟨ Z ⟩))) s1) ≡ ((XL ─ ((W ∙ ⟨ Z ⟩) ∙ Y)) s2))
                     → SCs XL ⇒ A → SCs XR ⇒ A
  R2` : ∀ {A W Y Z XL XR}
            {s1 : (⟨ W ⟩ ∙ (Y ∙ Z)) ∈s XR}
            {s2 : ((Y ∙ ⟨ W ⟩) ∙ Z) ∈s XL}
            (s3 : ((XR ─ (⟨ W ⟩ ∙ (Y ∙ Z))) s1) ≡ ((XL ─ ((Y ∙ ⟨ W ⟩) ∙ Z)) s2))
                     → SCs XL ⇒ A → SCs XR ⇒ A
