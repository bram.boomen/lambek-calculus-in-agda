open import Agda.Builtin.Equality using (_≡_; refl)

module NDProof where
open import Logic

infix 4 NLn_ Ln_ LPn_ SCn_
infix 5 _⊢_
data Natural : Set where
  _⊢_ : Struct → Type → Natural

data NLn_ : Natural → Set where
  ax : (A : Struct) → NLn A ⊢ [ A ]ᵀ
  ⟪E : ∀ {A B   X Y} → NLn X ⊢ B ⟪ A → NLn Y ⊢ A → NLn X ∙ Y ⊢ B
  ⟫E : ∀ {A B   X Y} → NLn X ⊢ A → NLn Y ⊢ A ⟫ B → NLn X ∙ Y ⊢ B
  ⊗E : ∀ {A B C   Y}{X[A∙B] X[Y]}{s1 : (A ∙ B) ∈s X[A∙B]}{s2 : Y ∈s X[Y]}
                     → (s3 : ((X[A∙B] ─ (A ∙ B)) s1) ≡ ((X[Y] ─ Y) s2))
                     → NLn Y ⊢ [ A ]ᵀ ⊗ [ B ]ᵀ → NLn X[A∙B] ⊢ C → NLn X[Y] ⊢ C
  ⟪I : ∀ {A B   X  } → NLn X ∙ A ⊢ B → NLn X ⊢ B ⟪ [ A ]ᵀ
  ⟫I : ∀ {A B   X  } → NLn A ∙ X ⊢ B → NLn X ⊢ [ A ]ᵀ ⟫ B
  ⊗I : ∀ {A B   X Y} → NLn X ⊢ A → NLn Y ⊢ B → NLn X ∙ Y ⊢ A ⊗ B

data Ln_ : Natural → Set where
  ax : (A : Struct) → Ln A ⊢ [ A ]ᵀ
  ⟪E : ∀ {A B   X Y} → Ln X ⊢ B ⟪ A → Ln Y ⊢ A → Ln X ∙ Y ⊢ B
  ⟫E : ∀ {A B   X Y} → Ln X ⊢ A → Ln Y ⊢ A ⟫ B → Ln X ∙ Y ⊢ B
  ⊗E : ∀ {A B C   Y}{X[A∙B] X[Y]}{s1 : (A ∙ B) ∈s X[A∙B]}{s2 : Y ∈s X[Y]}
                     → (s3 : ((X[A∙B] ─ (A ∙ B)) s1) ≡ ((X[Y] ─ Y) s2))
                     → Ln Y ⊢ [ A ]ᵀ ⊗ [ B ]ᵀ → Ln X[A∙B] ⊢ C → Ln X[Y] ⊢ C
  ⟪I : ∀ {A B   X  } → Ln X ∙ A ⊢ B → Ln X ⊢ B ⟪ [ A ]ᵀ
  ⟫I : ∀ {A B   X  } → Ln A ∙ X ⊢ B → Ln X ⊢ [ A ]ᵀ ⟫ B
  ⊗I : ∀ {A B   X Y} → Ln X ⊢ A → Ln Y ⊢ B → Ln X ∙ Y ⊢ A ⊗ B
  
  assᵣ : ∀ {Y Z W D XR XL}
           {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
           {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
           (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
               → Ln XL ⊢ D → Ln XR ⊢ D
  assₗ : ∀ {Y Z W D XR XL}
           {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
           {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
           (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
               → Ln XR ⊢ D → Ln XL ⊢ D

data LPn_ : Natural → Set where
  nln  : ∀ {Π} → Ln Π → LPn Π
  ax : (A : Struct) → LPn A ⊢ [ A ]ᵀ
  ⟪E : ∀ {A B   X Y} → LPn X ⊢ B ⟪ A → LPn Y ⊢ A → LPn X ∙ Y ⊢ B
  ⟫E : ∀ {A B   X Y} → LPn X ⊢ A → LPn Y ⊢ A ⟫ B → LPn X ∙ Y ⊢ B
  ⊗E : ∀ {A B C   Y}{X[A∙B] X[Y]}{s1 : (A ∙ B) ∈s X[A∙B]}{s2 : Y ∈s X[Y]}
                     → (s3 : ((X[A∙B] ─ (A ∙ B)) s1) ≡ ((X[Y] ─ Y) s2))
                     → LPn Y ⊢ [ A ]ᵀ ⊗ [ B ]ᵀ → LPn X[A∙B] ⊢ C → LPn X[Y] ⊢ C
  ⟪I : ∀ {A B   X  } → LPn X ∙ A ⊢ B → LPn X ⊢ B ⟪ [ A ]ᵀ
  ⟫I : ∀ {A B   X  } → LPn A ∙ X ⊢ B → LPn X ⊢ [ A ]ᵀ ⟫ B
  ⊗I : ∀ {A B   X Y} → LPn X ⊢ A → LPn Y ⊢ B → LPn X ∙ Y ⊢ A ⊗ B

  assᵣ : ∀ {Y Z W D XR XL}
           {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
           {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
           (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
               → LPn XL ⊢ D → LPn XR ⊢ D
  assₗ : ∀ {Y Z W D XR XL}
           {s2 : ((Y ∙ Z) ∙ W) ∈s XL}
           {s1 : (Y ∙ ( Z ∙ W)) ∈s XR}
           (s3 : ((XR ─ (Y ∙ (Z ∙ W))) s1) ≡ ((XL ─ ((Y ∙ Z) ∙ W)) s2))
               → LPn XR ⊢ D → LPn XL ⊢ D

  comm : ∀ {Y Z C}{X₁ X₂}{s1 : (Z ∙ Y) ∈s X₁}{s2 : (Y ∙ Z) ∈s X₂}
           (s3 : ((X₁ ─ (Z ∙ Y)) s1) ≡ ((X₂ ─ (Y ∙ Z)) s2))
               → LPn X₁ ⊢ C → LPn X₂ ⊢ C

data SCn_ : Natural → Set where
  ax : (A : Struct)  → SCn A ⊢ [ A ]ᵀ
  ⟪E : ∀ {A B   X Y} → SCn X ⊢ B ⟪ A → SCn Y ⊢ A → SCn X ∙ Y ⊢ B
  ⟫E : ∀ {A B   X Y} → SCn X ⊢ A → SCn Y ⊢ A ⟫ B → SCn X ∙ Y ⊢ B
  ⊗E : ∀ {A B C   Y}{X[A∙B] X[Y]}{s1 : (A ∙ B) ∈s X[A∙B]}{s2 : Y ∈s X[Y]}
                     → (s3 : ((X[A∙B] ─ (A ∙ B)) s1) ≡ ((X[Y] ─ Y) s2))
                     → SCn Y ⊢ [ A ]ᵀ ⊗ [ B ]ᵀ → SCn X[A∙B] ⊢ C → SCn X[Y] ⊢ C
  ◇E : ∀ {A B     Y}{X[⟨A⟩] X[Y]}{s1 : ⟨ A ⟩ ∈s X[⟨A⟩]}{s2 : Y ∈s X[Y]}
                     → (s3 : ((X[⟨A⟩] ─ ⟨ A ⟩) s1) ≡ ((X[Y] ─ Y) s2))
                     → SCn Y ⊢ ◇ [ A ]ᵀ → SCn X[⟨A⟩] ⊢ B → SCn X[Y] ⊢ B
  □E : ∀ {A     X  } → SCn X ⊢ □ A → SCn ⟨ X ⟩ ⊢ A
  ⟪I : ∀ {A B   X  } → SCn X ∙ A ⊢ B → SCn X ⊢ B ⟪ [ A ]ᵀ
  ⟫I : ∀ {A B   X  } → SCn A ∙ X ⊢ B → SCn X ⊢ [ A ]ᵀ ⟫ B
  ⊗I : ∀ {A B   X Y} → SCn X ⊢ A → SCn Y ⊢ B → SCn X ∙ Y ⊢ A ⊗ B
  ◇I : ∀ {A     X  } → SCn X ⊢ A → SCn ⟨ X ⟩ ⊢ ◇ A
  □I : ∀ {A     X  } → SCn ⟨ X ⟩ ⊢ A → SCn X ⊢ □ A

  R1  : ∀ {A W Y Z XL XR}{s1 : (W ∙ (Y ∙ ⟨ Z ⟩)) ∈s XR}{s2 : ((W ∙ Y) ∙ ⟨ Z ⟩) ∈s XL}
      → (s3 : ((XR ─ (W ∙ (Y ∙ ⟨ Z ⟩))) s1) ≡ ((XL ─ ((W ∙ Y) ∙ ⟨ Z ⟩)) s2))
      → SCn XR ⊢ A → SCn XL ⊢ A
  R1` : ∀ {A W Y Z XL XR}{s1 : (⟨ W ⟩ ∙ (Y ∙ Z)) ∈s XR}{s2 : ((⟨ W ⟩ ∙ Y) ∙ Z) ∈s XL}
      → (s3 : ((XR ─ (⟨ W ⟩ ∙ (Y ∙ Z))) s1) ≡ ((XL ─ ((⟨ W ⟩ ∙ Y) ∙ Z)) s2))
      → SCn XL ⊢ A → SCn XR ⊢ A
  R2  : ∀ {A W Y Z XL XR}{s1 : (W ∙ (Y ∙ ⟨ Z ⟩)) ∈s XR}{s2 : ((W ∙ ⟨ Z ⟩) ∙ Y) ∈s XL}
      → (s3 : ((XR ─ (W ∙ (Y ∙ ⟨ Z ⟩))) s1) ≡ ((XL ─ ((W ∙ ⟨ Z ⟩) ∙ Y)) s2))
      → SCn XL ⊢ A → SCn XR ⊢ A
  R2` : ∀ {A W Y Z XL XR}{s1 : (⟨ W ⟩ ∙ (Y ∙ Z)) ∈s XR}{s2 : ((Y ∙ ⟨ W ⟩) ∙ Z) ∈s XL}
      → (s3 : ((XR ─ (⟨ W ⟩ ∙ (Y ∙ Z))) s1) ≡ ((XL ─ ((Y ∙ ⟨ W ⟩) ∙ Z)) s2))
      → SCn XL ⊢ A → SCn XR ⊢ A

data Natural` : Set where
  _⊢_ : List → Type → Natural`

infix 4 Ln`_
data Ln`_ : Natural` → Set where
  ax : {X : List} → (A : Type) → A ∈l X → Ln` X ⊢ A
  ⟫E : ∀ {A B   X Y  } → Ln` X ⊢ A → Ln` Y ⊢ A ⟫ B → Ln` X ++ Y ⊢ B
  ⟪E : ∀ {A B   X Y  } → Ln` X ⊢ B ⟪ A → Ln` Y ⊢ A → Ln` X ++ Y ⊢ B
  ⊗E : ∀ {A B C X Y Z} → Ln` X ⊢ A ⊗ B → Ln` Y ++ (A ∷ ø ++ (B ∷ ø ++ Z)) ⊢ C
                                       → Ln` (X ++ Y) ++ Z ⊢ C
  ⟫I : ∀ {A B   X    } → Ln` A ∷ ø ++ X ⊢ B → Ln` X ⊢ A ⟫ B
  ⟪I : ∀ {A B   X    } → Ln` X ++ A ∷ ø ⊢ B → Ln` X ⊢ B ⟪ A
  ⊗I : ∀ {A B   X Y  } → Ln` X ⊢ A → Ln` Y ⊢ B → Ln` X ++ Y ⊢ A ⊗ B

data Natural`` : Set where
  _⊢_ : Multiset → Type → Natural``

infix 4 LPn`_
data LPn`_ : Natural`` → Set where
  ax : {X : Multiset}  → (A : Type) → A ∈m X → LPn` X ⊢ A
  ⊸E : ∀ {A B   X Y Z} → (p : (X ∪ Y) ⊂ Z)
                       → LPn` X ⊢ A ⊸ B → LPn` Y ⊢ A → LPn` Z ⊢ B
  ⊗E : ∀ {A B C X X` Y Z} → (q : (A , (B , X)) ⊂ X`) → (p : (X ∪ Y) ⊂ Z)
                       → LPn` Y ⊢ A ⊗ B → LPn` X` ⊢ C → LPn` Z ⊢ C
  ⊸I : ∀ {A B   X X` } → (p : (A , X`) ⊂ X)
                       → LPn` X ⊢ B → LPn` X` ⊢ A ⊸ B
  ⊗I : ∀ {A B   X Y Z} → (p : (X ∪ Y) ⊂ Z)
                       → LPn` X ⊢ A → LPn` Y ⊢ B → LPn` Z ⊢ A ⊗ B

Π-comm : ∀ {A Γ Γ`} → Γ ⊆ Γ` → LPn` Γ ⊢ A → LPn` Γ` ⊢ A
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (ax     A      A∈Γ  ) = ax A (Γ⊂Γ` A∈Γ)
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (⊸E  XY⊂Γ      π ρ) = ⊸E (⊂-trans XY⊂Γ Γ⊂Γ`) (Π-comm ⊆-refl π) (Π-comm ⊆-refl ρ)
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (⊗E ABX⊂X XY⊂Γ π ρ) = ⊗E ABX⊂X (⊂-trans XY⊂Γ Γ⊂Γ`) (Π-comm ⊆-refl π) (Π-comm ⊆-refl ρ)
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (⊸I  AΓ⊂X      π  ) = ⊸I (⊂-trans (⊂-weakⁱ Γ`⊂Γ) AΓ⊂X) (Π-comm ⊆-refl π)
Π-comm (Γ⊂Γ` & Γ`⊂Γ) (⊗I  XY⊂Γ      π ρ) = ⊗I (⊂-trans XY⊂Γ Γ⊂Γ`) (Π-comm ⊆-refl π) (Π-comm ⊆-refl ρ)
