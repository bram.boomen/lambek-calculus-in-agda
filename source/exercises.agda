module exercises where

open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import Logic
open import NDProof
open import Data.Nat

trivNLn : NLn ⌈ np ⌉ ∙ ⌈ np ⟫ s ⌉ ⊢ s
trivNLn = ⟫E
            (ax ⌈ np ⌉)
            (ax ⌈ np ⟫ s ⌉)

arg-lower : NLn ⌈ (np ⟫ s) ⟪ ((s ⟪ np) ⟫ s) ⌉ ⊢ (np ⟫ s) ⟪ np
arg-lower = ⟪I
              (⟫I
                (⟫E
                  ( ax ⌈ np ⌉ )
                  (⟪E
                    (ax ⌈ (np ⟫ s) ⟪ ((s ⟪ np) ⟫ s) ⌉)
                    (⟫I
                      (⟪E
                        (ax ⌈ s ⟪ np ⌉)
                        (ax ⌈ np ⌉))))))

trivLn : Ln ⌈ np ⌉ ∙ ⌈ np ⟫ s ⌉ ⊢ s
trivLn = ⟫E
           (ax ⌈ np ⌉)
           (ax ⌈ np ⟫ s ⌉)

-- helper patterns to make things more readable
pattern assr x = assᵣ refl x
pattern assl x = assₗ refl x

arg-raise : Ln ⌈ (np ⟫ s) ⟪ np ⌉ ⊢ (np ⟫ s) ⟪ ((s ⟪ np) ⟫ s)
arg-raise = ⟪I
              (⟫I
                (assr
                  (⟫E
                    (⟪I
                      (assl
                        (⟫E
                          (ax ⌈ np ⌉)
                          (⟪E
                            (ax ⌈ (np ⟫ s) ⟪ np ⌉)
                            (ax ⌈ np ⌉)))))
                  (ax ⌈ (s ⟪ np) ⟫ s ⌉))))

-- helper patterns to make things more readable
pattern axi a = ax a here
[_] : Type → List
[ t ] = t ∷ ø

arg-raise` : Ln` [ (np ⟫ s) ⟪ np ]  ⊢ (np ⟫ s) ⟪ ((s ⟪ np) ⟫ s)
arg-raise` = ⟪I (⟫I (⟫E (⟪I (⟫E (axi np) (⟪E (axi ((np ⟫ s) ⟪ np)) (axi np)))) (axi ((s ⟪ np) ⟫ s))))

non-trivial : ℕ → NLn ⌈ (s ⟪ (np ⟫ s)) ⟫ s ⌉ ⊢ (s ⟪ (np ⟫ s)) ⟫ s
non-trivial zero = ax ⌈ (s ⟪ (np ⟫ s)) ⟫ s ⌉
non-trivial (suc (zero)) = ⟫I
                (⟫E
                  (ax ⌈ s ⟪ (np ⟫ s) ⌉)
                  (ax ⌈ (s ⟪ (np ⟫ s)) ⟫ s ⌉))
non-trivial _ = ⟫I
                (⟪E
                  (ax ⌈ s ⟪ (np ⟫ s) ⌉)
                  (⟫I
                    (⟫E
                      (⟪I
                        (⟫E
                          (ax ⌈ np ⌉)
                          (ax ⌈ np ⟫ s ⌉)))
                    (ax ⌈ (s ⟪ (np ⟫ s)) ⟫ s ⌉))))

postulate
  A B C : Type

_ : LPn` ((B ⊸ C) , ((A ⊸ B) , (A , ø))) ⊢ C
_ = ⊸E _ (axi (B ⊸ C)) (⊸E _ (axi (A ⊸ B)) (axi A))

_ : LPn` ((A ⊸ B) , ((B ⊸ C) , (A , ø))) ⊢ C
_ = ⊸E _ (axi (B ⊸ C)) (⊸E _ (axi (A ⊸ B)) (axi A))
