I have written Overture[^1] to provide a standard-library for the extended Lambek Calculus. It was mainly an exercise to get to know Agda and to judge how suitable Agda is for formalizing type-logical grammars before trying to write methods for automated theorem-proving and type-inference for the extended Lambek Calculus. As far as I know the limited amount of work done on type-logical grammars in Agda is by Wen Kokke, whose work heavily inspired this project[^2].

Overture provides 4 modules:

-   [Logic.agda](pdf/Logic.pdf)
    
    This module provides the building blocks for the systems of inference that we want to implement. It defines Types, 3 types of Structure and functions to operate on those Types and Structures. Finally it provides some proofs to assert desirable properties of these data types.

-   [CombinatorialProof.agda](pdf/CombinatorialProof.pdf)
    
    The Combinatorial Proof module is the best place to start to understand how type-logical grammars are implemented in Agda because it is a simple proof system that can almost be implemented, as it is described formally, to the letter.

-   [NDProof.agda](pdf/NDProof.pdf)
    
    The NDProof module describes the rules of various systems of Natural Deduction. Apart from the systems including associativity, commutativity and structural control I have also included versions which leave associativity and commutativity implicit by using different kinds of structures.

-   [SequentProof.agda](pdf/SequentProof.pdf)
    
    The SequentProof module is similar to the NDProof module but describes the Sequent Calculus instead of Natural Deduction.

These modules are implemented as literate Agda-files, the documentation and in-depth explanation of the modules can be found in those files, which are hosted at [my Gitlab](https://gitlab.com/bram.boomen/lambek-calculus-in-agda/pdf).

The goal of these modules is to provide these proof-systems with a simple import statement. Also, the modules have been implemented utilizing the Agda standard library minimally, which makes it more transparent and specialized for use cases of type-logical grammars. As it is currently, these modules provide the types and tooling to interactively perform Lambek style proofs in Agda. I have included some examples below:

    arg-lower : NLn ⌈ (np ⟫ s) ⟪ ((s ⟪ np) ⟫ s) ⌉ ⊢ (np ⟫ s) ⟪ np
    arg-lower = ⟪I
                  (⟫I
                    (⟫E
                      ( ax ⌈ np ⌉ )
                      (⟪E
                        (ax ⌈ (np ⟫ s) ⟪ ((s ⟪ np) ⟫ s) ⌉)
                        (⟫I
                          (⟪E
                            (ax ⌈ s ⟪ np ⌉)
                            (ax ⌈ np ⌉))))))

    pattern assr x = assᵣ refl x
    pattern assl x = assₗ refl x
    
    arg-raise : Ln ⌈ (np ⟫ s) ⟪ np ⌉ ⊢ (np ⟫ s) ⟪ ((s ⟪ np) ⟫ s)
    arg-raise = ⟪I
                  (⟫I
                    (assr
                      (⟫E
                        (⟪I
                          (assl
                            (⟫E
                              (ax ⌈ np ⌉)
                              (⟪E
                                (ax ⌈ (np ⟫ s) ⟪ np ⌉)
                                (ax ⌈ np ⌉)))))
                      (ax ⌈ (s ⟪ np) ⟫ s ⌉))))

    pattern axi a = ax a here
    
    _ : LPn` ((B ⊸ C) , ((A ⊸ B) , (A , ø))) ⊢ C
    _ = ⊸E _
          (axi (B ⊸ C))
          (⊸E _
            (axi (A ⊸ B))
            (axi A))
    
    _ : LPn` ((A ⊸ B) , ((B ⊸ C) , (A , ø))) ⊢ C
    _ = ⊸E _
          (axi (B ⊸ C))
          (⊸E _
            (axi (A ⊸ B))
            (axi A))

# Footnotes

[^1]: The name is a pun on Prelude, a standard basic library for both Agda and Haskell.

[^2]: Wen Kokke. [Formalising type-logical grammars in agda.](https://arxiv.org/abs/1709.00728) arXiv preprint arXiv:1709.00728, 2017.
